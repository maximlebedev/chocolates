import java.util.Scanner;

/**
 * Класс для нахождения количества шоколадок которые можно позволить
 *
 * @author M.Lebedev 17IT18
 */
public class Chocolates {
    public static void main(String[] args) {
        try {
            System.out.println("Правило ввода: Входные данные являются целыми и больше 1.\n");
            Scanner scanner = new Scanner(System.in);

            System.out.println("Сколько у вас денег? ");
            int money = scanner.nextInt();
            if (money <= 1) {
                throw new IncorrectDataException();
            }

            System.out.println("Какая цена на шоколадку? ");
            int price = scanner.nextInt();
            if (price == 0) {
                throw new IncorrectDataException();
            }

            System.out.println("За сколько обёрток дают новую шоколадку?");
            int wrap = scanner.nextInt();
            if (wrap <= 1) {
                throw new IncorrectDataException();
            }

            int chocolates = findMaxChocolate(money, price, wrap);

            System.out.println("Максимальное количество шоколадок: " + chocolates);
        } catch (IncorrectDataException e) {
            System.err.println("Некорректно введённые данные.");
        }
    }

    /**
     * Метод возвращает максимальное количесво шоколадок, которые мы можем получить
     *
     * @param money деньги
     * @param price цена на шоколадку
     * @param wrap  обёрток для получения шоколадки
     * @return максимальное количество
     */
    public static int findMaxChocolate(int money, int price, int wrap) {
        int chocolates = 0;
        int countWrap = 0;
        int countBuy = money / price;

        for (int i = 0; i < countBuy; i++) {
            chocolates++;
            countWrap++;
            if (wrap == countWrap) {
                chocolates++;
                countWrap = 1;
            }

            if (wrap == countWrap) {
                chocolates++;
            }
        }

        return chocolates;
    }
}
